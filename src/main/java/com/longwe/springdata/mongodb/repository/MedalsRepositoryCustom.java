/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

/**
 * @author Miya W. Longwe
 *
 */
public interface MedalsRepositoryCustom {
	
	/**
	 * Gets the bronze count.
	 *
	 * @param countryName the country name
	 * @return the bronze count
	 */
	public int getBronzeCount(String countryName);

    /**
     * Gets the silver count.
     *
     * @param countryName the country name
     * @return the silver count
     */
    public int getSilverCount(String countryName);
    
    /**
     * Gets the gold count.
     *
     * @param countryName the country name
     * @return the gold count
     */
    public int getGoldCount(String countryName);


}
