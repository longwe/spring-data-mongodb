/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

/**
 * @author mlongwe
 *
 */
public interface RepositoryPackage { // This just a no marker repository used to help in the component scanning 
}
