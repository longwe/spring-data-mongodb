package com.longwe.springdata.mongodb.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * The default configuration loading the Spring Boot context.
 * 
 * @author Miya W Longwe
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.longwe.springdata.mongodb")
public class DefaultConfig {

}