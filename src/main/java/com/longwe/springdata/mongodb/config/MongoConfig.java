/**
 * 
 */
package com.longwe.springdata.mongodb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.longwe.springdata.mongodb.repository.RepositoryPackage;
import com.longwe.springdata.mongodb.template.TemplatePackage;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

/**
 * @author Miya W. Longwe
 * 
 *  This class represent the base configuration of mongodb 
 * AbstractMongoConfiguration provides a default set of infrastructure components
   and provides callback methods to tweak the configuration as necessary
 *
 */
/*
 *@EnableMongoRepositories activates MongoDB repository infrastructure.
basePackageClasses specifies the package(s) to scan for the 
repositories@EnableMongoRepositories activates MongoDB repository infrastructure.
basePackageClasses specifies the package(s) to scan for the repositories
 */
@Configuration
@EnableMongoRepositories(basePackageClasses=RepositoryPackage.class)
@ComponentScan(basePackageClasses=TemplatePackage.class)
public class MongoConfig  extends AbstractMongoConfiguration {

	@Override
	protected String getDatabaseName() {
		
		return "java-meetup-world";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#mongo()
	 */
	public Mongo mongo() throws Exception {
		   /*mongo() creates a MongoClient object which contains an internal connection pool.
		 *  This connects to the database returned by getDatabaseName() 
		 *  on the specified host using the default port (27017)
		 */
		MongoClient mongoClient2 = new MongoClient( "localhost" , 27017 );
		
		/*
		MongoClient mongoClient = new MongoClient(Arrays.asList(
				   new ServerAddress("localhost", 27017),
				   new ServerAddress("localhost", 27018),
				   new ServerAddress("localhost", 27019)));
			*/	 
		//MongoClient mongoClient =  new  MongoClient("localhost");
		mongoClient2.setWriteConcern(WriteConcern.SAFE);
		return mongoClient2;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#getMappingBasePackage()
	 */
    protected String getMappingBasePackage() {
		/*
		 * getMappingBasePackage() method returns the base package to scan for mapped Documents
		 */
        return "com.longwe.springdata.mongodb.domain";
    }
    
// ------------- MongoTemplate -------------------------
    /*
     * (non-Javadoc)
     * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#mongoTemplate()
     */
    @Bean
	public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), getDatabaseName());
    }


}
