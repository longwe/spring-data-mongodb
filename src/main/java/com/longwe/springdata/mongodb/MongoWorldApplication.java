/**
 * 
 */
package com.longwe.springdata.mongodb;

import java.util.Arrays;

import javax.inject.Inject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoOperations;

import com.longwe.springdata.mongodb.config.DefaultConfig;
import com.longwe.springdata.mongodb.domain.Continent;
import com.longwe.springdata.mongodb.domain.State;
import com.longwe.springdata.mongodb.repository.ContinentRepository;
import com.longwe.springdata.mongodb.repository.CountryRepository;

/**
 * @author mlongwe
 * 
 */
@Import(DefaultConfig.class)
public class MongoWorldApplication implements CommandLineRunner {

	@Inject
	ContinentRepository continentRepository;
	@Inject
	CountryRepository countryRepository;
	@Inject
	private MongoOperations mongoOps;

	public static void main(String... args) {
		SpringApplication.run(MongoWorldApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("**** Loading database with Continent data");
		setupTestData();
		System.out.println("**** Completed Loading database with Continent data");
		testDeleteUsingId();
		setTestStateData();
		

	}

	private void testDeleteUsingId() {

		continentRepository.delete(3L);

		if (mongoOps.findById(3L, Continent.class)==(null)){
        	System.out.println("Document with id 3L has been deleted");
        };
	}

	private void setupTestData() {
		if (mongoOps.collectionExists(Continent.class)) {
			mongoOps.dropCollection(Continent.class);
		}

		Continent[] continents = new Continent[] {
				new Continent(6, "Australia"), new Continent(1, "Africa"),
				new Continent(3, "Europe"), new Continent(2, "Asia"),
				new Continent(4, "North America"),
				new Continent(5, "South America")

		};

		mongoOps.insertAll(Arrays.asList(continents));
	}
	
	private void setTestStateData(){
    	if (mongoOps.collectionExists(State.class)) {
            mongoOps.dropCollection(State.class);
        }
    	 State[] continents = new State[] {
 	        	new State (	 "Alabama",  "AL",  1819,  "Montgomery",  1846 ),
 	        	new State (  "Alaska",  "AK",  1959,  "Juneau",  1906 ),
 	        	new State ( "Arizona",  "AZ",  1912,  "Phoenix",  1889 ),
 	        	new State (  "Arkansas",  "AR",  1836,  "Little Rock",  1821 ),
 	        	new State (  "California",  "CA",  1850,  "Sacramento",  1854 ),
 	        	new State (  "Colorado",  "CO",  1876,  "Denver",  1867 ),
 	        	new State (  "Connecticut",  "CT",  1788,  "Hartford",  1875 ),
 	        	new State (  "Delaware",  "DE",  1787,  "Dover",  1777 ),
 	        	new State (    "Florida",  "FL",  1845,  "Tallahassee",  1824 ),
 	        	new State (  "Georgia",  "GA",  1788,  "Atlanta",  1868 ),
 	        	new State (  "Hawaii",  "HI",  1959,  "Honolulu",  1845 ),
 	        	new State (  "Idaho",  "ID",  1890,  "Boise",  1865 ),
 	        	new State (  "Illinois",  "IL",  1818,  "Springfield",  1837 ),
 	        	new State (  "Indiana",  "IN",  1816,  "Indianapolis",  1825 ),
 	        	new State (  "Iowa",  "IA",  1846,  "Des Moines",  1857 ),
 	        	new State (     "Kansas",  "KS",  1861,  "Topeka",  1856 ),
 	        	new State (  "Kentucky",  "KY",  1792,  "Frankfort",  1792 ),
 	        	new State (  "Louisiana",  "LA",  1812,  "Baton Rouge",  1880 ),
 	        	new State (  "Maine",  "ME",  1820,  "Augusta",  1832 ),
 	        	new State ( "Maryland",  "MD",  1788,  "Annapolis",  1694 ),
 	        	new State (  "Massachusetts",  "MA",  1788,  "Boston",  1630 ),
 	        	new State (  "Michigan",  "MI",  1837,  "Lansing",  1847 ),
 	        	new State (  "Minnesota",  "MN",  1858,  "Saint Paul",  1849 ),
 	        	new State (  "Mississippi",  "MS",  1817,  "Jackson",  1821 ),
 	        	new State (  "Missouri",  "MO",  1821,  "Jefferson City",  1826 ),
 	        	new State (  "Montana",  "MT",  1889,  "Helena",  1875 ),
 	        	new State (  "Nebraska",  "NE",  1867,  "Lincoln",  1867 ),
 	        	new State (  "Nevada",  "NV",  1864,  "Carson City",  1861 ),
 	        	new State (  "New Hampshire",  "NH",  1788,  "Concord",  1808 ),
 	        	new State (  "New Jersey",  "NJ",  1787,  "Trenton",  1784 ),
 	        	new State (  "New Mexico",  "NM",  1912,  "Santa Fe",  1610 ),
 	        	new State (  "New York",  "NY",  1788,  "Albany",  1797 ),
 	        	new State (  "North Carolina",  "NC",  1789,  "Raleigh",  1792 ),
 	        	new State (  "North Dakota",  "ND",  1889,  "Bismarck",  1883 ),
 	        	new State (  "Ohio",  "OH",  1803,  "Columbus",  1816 ),
 	        	new State (  "Oklahoma",  "OK",  1907,  "Oklahoma City",  1910 ),
 	        	new State (  "Oregon",  "OR",  1859,  "Salem",  1855 ),
 	        	new State (  "Pennsylvania",  "PA",  1787,  "Harrisburg",  1812 ),
 	        	new State (  "Rhode Island",  "RI",  1790,  "Providence",  1900 ),
 	        	new State (  "South Carolina",  "SC",  1788,  "Columbia",  1786 ),
 	        	new State (  "South Dakota",  "SD",  1889,  "Pierre",  1889 ),
 	        	new State (  "Tennessee",  "TN",  1796,  "Nashville",  1826 ),
 	        	new State (  "Texas",  "TX",  1845,  "Austin",  1839 ),
 	        	new State (  "Utah",  "UT",  1896,  "Salt Lake City", 1858 ),
 	        	new State (  "Vermont",  "VT",  1791,  "Montpelier",  1805 ),
 	        	new State (  "Virginia",  "VA",  1788,  "Richmond",  1780 ),
 	        	new State (  "Washington",  "WA",  1889,  "Olympia",  1853 ),
 	        	new State (  "West Virginia",  "WV",  1863,  "Charleston",  1885 ),
 	        	new State (  "Wisconsin",  "WI",  1848,  "Madison",  1838 ),
 	        	new State (  "Wyoming",  "WY",  1890,  "Cheyenne",  1869 ),
 	           
 	  
 	        };
       

        mongoOps.insertAll(Arrays.asList(continents));
    }

}
