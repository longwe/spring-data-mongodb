/**
 * 
 */
package com.longwe.springdata.mongodb.domain;

/**
 * @author mlongwe
 *
 */
public class Continent {
	private final long id;
    private final String name;
    
    public Continent(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public String toString() {
        return this.getName();
    }


}
