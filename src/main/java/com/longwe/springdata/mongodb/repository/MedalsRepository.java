/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

import java.math.BigInteger;

import org.springframework.data.repository.Repository;

import com.longwe.springdata.mongodb.domain.OlympicMedals;

/**
 * @author Miya W. Longwe
 *
 */

public interface MedalsRepository  extends Repository <OlympicMedals, BigInteger>,
MedalsRepositoryCustom {
    
public OlympicMedals findByCountryName(String name);

}
