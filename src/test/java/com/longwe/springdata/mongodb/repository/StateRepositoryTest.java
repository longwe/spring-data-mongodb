/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.longwe.springdata.mongodb.config.DefaultConfig;
import com.longwe.springdata.mongodb.domain.State;



/**
 * @author mlongwe
 *
 */@ActiveProfiles("test")
 @RunWith(SpringJUnit4ClassRunner.class)
 @ContextConfiguration(classes = { DefaultConfig.class }, loader = SpringApplicationContextLoader.class)
	    

public class StateRepositoryTest {
	 @Autowired private StateRepository stateRepo;
	 @Autowired private MongoOperations mongoOps;
	    
	    @Before
	    public void reset() {
	    	setTestStateData();
	    }
	    
	    
	    
	    //------------------------------------------------- paging to list
	    
	    @Test
	    public void testPagingToList() {
	        int page = 4;
	        int pageSize = 4;
	        
	        Pageable pageable = new PageRequest(page, pageSize);
	        List<State> states = stateRepo.findByCapitalSinceGreaterThan(1800, pageable);
	        
	        assertThat(states.toString(), equalTo("[Maine, Michigan, Minnesota, Mississippi]"));
	    }
	    
	    //------------------------------------------------- sorting all
	    
	    @Test
	    public void testSortingAll() {

	        Sort sort = new Sort(Direction.ASC, "abbreviation");
	        Iterable<State> states = stateRepo.findAll(sort);
	        
	        StringBuffer abbreviations = new StringBuffer();
	        for (State state : states) {
	            abbreviations.append(state.getAbbreviation() + ";");
	        }
	        
	        assertThat(abbreviations.toString(), equalTo("AK;AL;AR;AZ;CA;CO;CT;DE;FL;GA;HI;IA;ID;IL;IN;KS;KY;LA;MA;" +
	                                                 "MD;ME;MI;MN;MO;MS;MT;NC;ND;NE;NH;NJ;NM;NV;NY;OH;OK;OR;PA;" +
	                                                 "RI;SC;SD;TN;TX;UT;VA;VT;WA;WI;WV;WY;"));
	    }
	    
	    //------------------------------------------------- sorting some
	    
	    @Test
	    public void testSortingSome() {
	        
	        Sort sort = new Sort(Direction.DESC, "dateOfStatehood");
	        Iterable<State> states = stateRepo.findByDateOfStatehoodGreaterThan(1900, sort);
	        
	        StringBuffer abbreviations = new StringBuffer();
	        for (State state : states) {
	            abbreviations.append(state.getDateOfStatehood() + ";");
	        }
	        
	        assertThat(abbreviations.toString(), equalTo("1959;1959;1912;1912;1907;"));
	    }
	    
	    //------------------------------------------------- paging and sorting
	    
	    @Test
	    public void testPagingAndSorting() {
	        int page = 3;
	        int pageSize = 6;
	        
	        Pageable pageable = new PageRequest(page, pageSize, Direction.ASC, "capitalSince");
	        Page<State> states = stateRepo.findAll(pageable);
	        
	        assertThat(states.getContent().toString(), equalTo("[Missouri, Tennessee, Maine, Illinois, Wisconsin, Texas]"));
	    }
	    
	    private void setTestStateData(){
	    	if (mongoOps.collectionExists(State.class)) {
	            mongoOps.dropCollection(State.class);
	        }
	    	 State[] continents = new State[] {
	 	        	new State (	 "Alabama",  "AL",  1819,  "Montgomery",  1846 ),
	 	        	new State (  "Alaska",  "AK",  1959,  "Juneau",  1906 ),
	 	        	new State ( "Arizona",  "AZ",  1912,  "Phoenix",  1889 ),
	 	        	new State (  "Arkansas",  "AR",  1836,  "Little Rock",  1821 ),
	 	        	new State (  "California",  "CA",  1850,  "Sacramento",  1854 ),
	 	        	new State (  "Colorado",  "CO",  1876,  "Denver",  1867 ),
	 	        	new State (  "Connecticut",  "CT",  1788,  "Hartford",  1875 ),
	 	        	new State (  "Delaware",  "DE",  1787,  "Dover",  1777 ),
	 	        	new State (    "Florida",  "FL",  1845,  "Tallahassee",  1824 ),
	 	        	new State (  "Georgia",  "GA",  1788,  "Atlanta",  1868 ),
	 	        	new State (  "Hawaii",  "HI",  1959,  "Honolulu",  1845 ),
	 	        	new State (  "Idaho",  "ID",  1890,  "Boise",  1865 ),
	 	        	new State (  "Illinois",  "IL",  1818,  "Springfield",  1837 ),
	 	        	new State (  "Indiana",  "IN",  1816,  "Indianapolis",  1825 ),
	 	        	new State (  "Iowa",  "IA",  1846,  "Des Moines",  1857 ),
	 	        	new State (     "Kansas",  "KS",  1861,  "Topeka",  1856 ),
	 	        	new State (  "Kentucky",  "KY",  1792,  "Frankfort",  1792 ),
	 	        	new State (  "Louisiana",  "LA",  1812,  "Baton Rouge",  1880 ),
	 	        	new State (  "Maine",  "ME",  1820,  "Augusta",  1832 ),
	 	        	new State ( "Maryland",  "MD",  1788,  "Annapolis",  1694 ),
	 	        	new State (  "Massachusetts",  "MA",  1788,  "Boston",  1630 ),
	 	        	new State (  "Michigan",  "MI",  1837,  "Lansing",  1847 ),
	 	        	new State (  "Minnesota",  "MN",  1858,  "Saint Paul",  1849 ),
	 	        	new State (  "Mississippi",  "MS",  1817,  "Jackson",  1821 ),
	 	        	new State (  "Missouri",  "MO",  1821,  "Jefferson City",  1826 ),
	 	        	new State (  "Montana",  "MT",  1889,  "Helena",  1875 ),
	 	        	new State (  "Nebraska",  "NE",  1867,  "Lincoln",  1867 ),
	 	        	new State (  "Nevada",  "NV",  1864,  "Carson City",  1861 ),
	 	        	new State (  "New Hampshire",  "NH",  1788,  "Concord",  1808 ),
	 	        	new State (  "New Jersey",  "NJ",  1787,  "Trenton",  1784 ),
	 	        	new State (  "New Mexico",  "NM",  1912,  "Santa Fe",  1610 ),
	 	        	new State (  "New York",  "NY",  1788,  "Albany",  1797 ),
	 	        	new State (  "North Carolina",  "NC",  1789,  "Raleigh",  1792 ),
	 	        	new State (  "North Dakota",  "ND",  1889,  "Bismarck",  1883 ),
	 	        	new State (  "Ohio",  "OH",  1803,  "Columbus",  1816 ),
	 	        	new State (  "Oklahoma",  "OK",  1907,  "Oklahoma City",  1910 ),
	 	        	new State (  "Oregon",  "OR",  1859,  "Salem",  1855 ),
	 	        	new State (  "Pennsylvania",  "PA",  1787,  "Harrisburg",  1812 ),
	 	        	new State (  "Rhode Island",  "RI",  1790,  "Providence",  1900 ),
	 	        	new State (  "South Carolina",  "SC",  1788,  "Columbia",  1786 ),
	 	        	new State (  "South Dakota",  "SD",  1889,  "Pierre",  1889 ),
	 	        	new State (  "Tennessee",  "TN",  1796,  "Nashville",  1826 ),
	 	        	new State (  "Texas",  "TX",  1845,  "Austin",  1839 ),
	 	        	new State (  "Utah",  "UT",  1896,  "Salt Lake City", 1858 ),
	 	        	new State (  "Vermont",  "VT",  1791,  "Montpelier",  1805 ),
	 	        	new State (  "Virginia",  "VA",  1788,  "Richmond",  1780 ),
	 	        	new State (  "Washington",  "WA",  1889,  "Olympia",  1853 ),
	 	        	new State (  "West Virginia",  "WV",  1863,  "Charleston",  1885 ),
	 	        	new State (  "Wisconsin",  "WI",  1848,  "Madison",  1838 ),
	 	        	new State (  "Wyoming",  "WY",  1890,  "Cheyenne",  1869 ),
	 	           
	 	  
	 	        };
	       

	        mongoOps.insertAll(Arrays.asList(continents));
	    }
	    	
	    }



