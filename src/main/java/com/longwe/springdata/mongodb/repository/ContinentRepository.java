/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.longwe.springdata.mongodb.domain.Continent;

/**
 * @author mlongwe
 *
 */
public interface ContinentRepository  extends CrudRepository<Continent, Long>{
	public Continent findByName(String name);

    public List<Continent> findByNameStartingWithIgnoreCase(String start);


}
