/**
 * 
 */
package com.longwe.springdata.mongodb.domain;

import java.math.BigInteger;

import org.springframework.data.annotation.Id;

/**
 * @author mlongwe
 *
 */
public class AbstractDocument {
	
	@Id private BigInteger documentId;

    public void setId(BigInteger id) {
        this.documentId = id;
    }

    public BigInteger getId() {
        return documentId;
    }

}
