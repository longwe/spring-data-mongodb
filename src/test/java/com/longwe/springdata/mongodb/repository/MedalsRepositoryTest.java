/**
 * 
 */
package com.longwe.springdata.mongodb.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.longwe.springdata.mongodb.config.DefaultConfig;
import com.longwe.springdata.mongodb.domain.OlympicMedals;
import com.longwe.springdata.mongodb.domain.OlympicMedals.MedalType;


/**
 * @author mlongwe
 *
 *
 *Using Spring Boot to load test prerequisites
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DefaultConfig.class }, loader = SpringApplicationContextLoader.class)

public class MedalsRepositoryTest {
	 @Autowired private MedalsRepository medalsRepo;
	 @Autowired private MongoOperations mongoOps;
	    
	    @Before
	    public void reset() {
	    	setupMedalTestData();
	    }
	    
	    //------------------------------------------------- manual implementation

	    @Test
	    public void testMedalCountReturnsCorrectNumber() {
	        assertThat(medalsRepo.getBronzeCount("China"), equalTo(201));
	        assertThat(medalsRepo.getSilverCount("East Germany"), equalTo(129));
	        assertThat(medalsRepo.getGoldCount("Italy"), equalTo(185));
	    }
	    
	    //------------------------------------------------- spring implementation
	    
	    @Test
	    public void testFindByCountryNameReturnsCorrectDocument() {
	        OlympicMedals medals = medalsRepo.findByCountryName("China");
	        assertThat(medals.getMedalCount(MedalType.GOLD), equalTo(128));
	    }
	    
	    private void setupMedalTestData () {
	        if (mongoOps.collectionExists(OlympicMedals.class)) {
	            mongoOps.dropCollection(OlympicMedals.class);
	        }
	        
	        OlympicMedals[] countries = new OlympicMedals[] {
	   new OlympicMedals ("United States",   666, 758, 976  ),
       new OlympicMedals( "Soviet Union",   296, 319, 395 ),
        new OlympicMedals( "Great Britain",   272, 272, 236  ),
         new OlympicMedals(    "France",   246, 223, 202  ),
       new OlympicMedals  ("Germany",   217, 182, 174  ),
       new OlympicMedals ( "Italy",   185, 166, 198  ),
        new OlympicMedals("Sweden",   176, 164, 143  ),
      new OlympicMedals(   "East Germany",   127, 129, 153 ),
      new OlympicMedals(   "China",   128, 144, 201  ),
      new OlympicMedals(    "Russia",   142, 122, 133  ),
      new OlympicMedals(   "Hungary",   165, 144, 167  ),
      new OlympicMedals(   "Australia",   177, 153, 138  )
	        };
	        mongoOps.insertAll(Arrays.asList(countries));
	    }

}
